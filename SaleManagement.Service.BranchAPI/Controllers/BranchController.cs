﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SaleManagement.Service.BranchAPI.Data;
using SaleManagement.Service.BranchAPI.Models;
using SaleManagement.Service.BranchAPI.Models.DTO;
using SaleManagement.Utility;
using System.Data;

namespace SaleManagement.Service.BranchAPI.Controllers
{
    [Route("api/branch")]
    [ApiController]
    public class BranchController : ControllerBase
    {
        private readonly AppDbContext _db;
        private ResponseDTO _response;
        private IMapper _mapper;
        public BranchController(AppDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
            _response = new ResponseDTO();
        }
        [HttpGet]
        public ResponseDTO GetAllBranch() {
            try
            {
                IEnumerable<Branch> branchList = _db.Branches.ToList(); 
                _response.Result = _mapper.Map<IEnumerable<Branch>>(branchList);
            } catch (Exception ex) { 
               _response.IsSuccess = false; 
                _response.Message = ex.Message; 
            }
            return _response;   
        }

        [HttpGet]
        [Route("GetBranchById/{name}")]
        public ResponseDTO GetBranchByBranchId(string name)
        {
            try
            {
                Branch product = _db.Branches.First(b => b.BranchId == name);
                _response.Result = _mapper.Map<BranchDTO>(product);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }
        [HttpPost("CreateBranch")]
        public ResponseDTO AddNewBranch([FromBody] BranchDTO branchDTO)
        {
            try
            {
                Branch obj = _mapper.Map<Branch>(branchDTO);
                _db.Branches.Add(obj);
                _db.SaveChanges();

                _response.Result = _mapper.Map<BranchDTO>(obj);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }

        [HttpPut]
        public ResponseDTO UpdateBrach([FromBody]Branch branch)
        {
            try
            {
                Branch obj = _mapper.Map<Branch>(branch);
                _db.Branches.Update(obj);
                _db.SaveChanges();

                _response.Result = _mapper.Map<BranchDTO>(branch);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }

        [HttpDelete]
        [Route("DeleteBranch/{name}")]
        public ResponseDTO DeleteBranch(string name)
        {
            try
            {
                Branch product = _db.Branches.First(b => b.BranchId == name);
                _db.Remove(product);
                _db.SaveChanges();
                _response.Result = _mapper.Map<BranchDTO>(product);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }
    }
}
