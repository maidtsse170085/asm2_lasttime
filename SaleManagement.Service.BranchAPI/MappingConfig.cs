﻿using AutoMapper;
using SaleManagement.Service.BranchAPI.Models;
using SaleManagement.Service.BranchAPI.Models.DTO;

namespace SaleManagement.Service.BranchAPI
{
    public class MappingConfig
    {
        public static MapperConfiguration RegisterMaps()
        {
            var mappingCongif = new MapperConfiguration(config => {
                config.CreateMap<Branch, BranchDTO>();
                config.CreateMap<BranchDTO, Branch>();
            });
            return mappingCongif;
        }
    }
}
