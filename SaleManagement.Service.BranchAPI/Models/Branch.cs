﻿using System.ComponentModel.DataAnnotations;

namespace SaleManagement.Service.BranchAPI.Models
{
    public class Branch
    {
        [Key]
        public string BranchId { get; set; }
        public string Name { get; set; }    
        public string Address { get; set; } 
        public string City { get; set; }    
        public string State { get; set; }   
        public string ZipCode { get; set; }  
    }
}
