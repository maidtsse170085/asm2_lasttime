﻿using SaleManagement.Utility;
using static SaleManagement.Utility.SD;

namespace SaleManagement.Models
{
    public class RequestDTO
    {
        public ApiType ApiType { get; set; } = ApiType.GET;
        public string Url { get; set; }
        public object Data { get; set; }
        public string AccessToken { get; set; }
    }
}