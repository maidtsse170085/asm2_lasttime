﻿using SaleManagement.Models;
using SaleManagement.Service.IService;
using SaleManagement.Utility;

namespace SaleManagement.Service
{
    public class BranchService : IBranchService
    {
        private readonly IBaseService _baseService;
        public BranchService(IBaseService baseService)
        {
            _baseService = baseService;
        }
        public async Task<ResponseDTO?> AddNewBranch(BranchDTO branchDTO)
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.POST,
                Data = branchDTO,
                Url = SD.BranchAPI + "/api/branch/CreateBranch"
            });

        }
        public async Task<ResponseDTO?> DeleteBranch(string name)
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.DELETE,
                Url = SD.BranchAPI + "/api/branch/DeleteBranch/" + name
            });
        }

        public async Task<ResponseDTO?> GetAllBranchAsync()
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.GET,
                Url = SD.BranchAPI + "/api/branch"
            });
        }


        public async Task<ResponseDTO?> GetBranchById(string name)
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.GET,
                Url = SD.BranchAPI + "/api/branch/GetBranchById/" + name
            });
        }

        public async Task<ResponseDTO?> UpdateBranch(BranchDTO branchDTO)
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.PUT,
                Data = branchDTO,
                Url = SD.BranchAPI + "/api/branch"
            });
        }
    }
}
