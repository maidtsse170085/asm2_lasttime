﻿using SaleManagement.Models;

namespace SaleManagement.Service.IService
{
    public interface IBaseService
    {
        Task<ResponseDTO> SendAsync(RequestDTO requestDto, bool withBearer = true);
    }
}
