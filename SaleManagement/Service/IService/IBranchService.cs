﻿using SaleManagement.Models;

namespace SaleManagement.Service.IService
{
    public interface IBranchService
    {
        Task<ResponseDTO?> GetAllBranchAsync();
        Task<ResponseDTO?> GetBranchById(string name);
        Task<ResponseDTO?> UpdateBranch(BranchDTO branchDTO);
        Task<ResponseDTO?> DeleteBranch(string name);
        Task<ResponseDTO?> AddNewBranch(BranchDTO branchDTO);
    }
}
